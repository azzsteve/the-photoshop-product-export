package cloudinary

import (
	"context"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"path"
	"time"
)

const resourcesImagesEndpoint = "resources/image"

// Images represent the cloudinary.Images response.
type Images struct {
	Resources []struct {
		AssetID      string    `json:"asset_id"`
		PublicID     string    `json:"public_id"`
		Format       string    `json:"format"`
		Version      int       `json:"version"`
		ResourceType string    `json:"resource_type"`
		Type         string    `json:"type"`
		CreatedAt    time.Time `json:"created_at"`
		Bytes        int       `json:"bytes"`
		Width        int       `json:"width"`
		Height       int       `json:"height"`
		URL          string    `json:"url"`
		SecureURL    string    `json:"secure_url"`
	} `json:"resources"`
}

// Images get a list of all the images that are available inside of the
// cloudinary account.
//
// Documentation: https://cloudinary.com/documentation/admin_api#get_resources
func (c *Cloudinary) Images(ctx context.Context) (Images, error) {
	imagesURL := c.baseURL
	imagesURL.Path = path.Join(imagesURL.Path, resourcesImagesEndpoint)
	q := imagesURL.Query()
	q.Add("max_results", "500")
	imagesURL.RawQuery = q.Encode()

	c.logger.Info(imagesURL.String())

	req, err := http.NewRequestWithContext(ctx, http.MethodGet, imagesURL.String(), nil)
	if err != nil {
		return Images{}, fmt.Errorf("creating new request for resources/image: %w", err)
	}

	startTime := time.Now()
	resp, err := c.client.Do(req)
	if err != nil {
		return Images{}, fmt.Errorf("client.Do for resources/image: %w", err)
	}
	defer resp.Body.Close()

	c.logger.Info("Sent HTTP request", "endpoint", resourcesImagesEndpoint, "duration_ms", time.Now().Sub(startTime).Milliseconds())

	b, err := io.ReadAll(resp.Body)
	if err != nil {
		return Images{}, fmt.Errorf("reading resources/image response body: %w", err)
	}

	images := Images{}

	err = json.Unmarshal(b, &images)
	if err != nil {
		return Images{}, fmt.Errorf("marshaling response body: %w", err)
	}

	return images, nil
}
