package cloudinary

import (
	"fmt"
	"net/http"
	"net/url"
	"time"

	"github.com/go-logr/logr"
	"github.com/go-logr/zapr"
	"go.uber.org/zap"
)

// Cloudinary is the client that interacts with the cloudinary API
// https://cloudinary.com/documentation/admin_api
type Cloudinary struct {
	baseURL *url.URL
	client  http.Client

	logger logr.Logger
}

// New creates a new instance of the Cloudinary api client.
func New(apiKey string, apiSecret string, cloudName string, options ...func(cloudinary *Cloudinary)) (Cloudinary, error) {
	client := Cloudinary{}

	for _, option := range options {
		option(&client)
	}

	if client.logger == nil {
		client.logger = zapr.NewLogger(zap.NewNop())
	}
	client.logger = client.logger.WithValues("cloud_name", cloudName)

	rawURL := fmt.Sprintf(
		"https://%s:%s@api.cloudinary.com/v1_1/%s",
		apiKey,
		apiSecret,
		cloudName,
	)

	baseURL, err := url.Parse(rawURL)
	if err != nil {
		return Cloudinary{}, fmt.Errorf("parsing Cloudinary base url: %w", err)
	}

	httpClient := http.Client{
		Timeout: 10 * time.Second,
	}

	client.client = httpClient
	client.baseURL = baseURL

	return client, nil
}

// WithLogr specifies logr.Logger to be used.
func WithLogr(log logr.Logger) func(cloudinary *Cloudinary) {
	return func(c *Cloudinary) {
		c.logger = log
	}
}
