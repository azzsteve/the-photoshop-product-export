package main

import (
	"context"
	"fmt"
	"os"
	"strings"

	"github.com/go-logr/logr"
	"github.com/go-logr/zapr"
	"go.uber.org/zap"

	"gitlab.com/azzsteve/the-photoshop-product-export/internal/cloudinary"
)

func main() {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	var logger logr.Logger

	zapLog, err := zap.NewProduction()
	if err != nil {
		panic(fmt.Sprintf("failed to start logger (%v)", err))
	}
	logger = zapr.NewLogger(zapLog)

	api, err := cloudinary.New(
		os.Getenv("CLOUDINARY_API_KEY"),
		os.Getenv("CLOUDINARY_API_SECRET"),
		os.Getenv("CLOUDINARY_CLOUD_NAME"),
		cloudinary.WithLogr(logger),
	)
	if err != nil {
		logger.Error(err, "Failed to create Cloudinary client")
		os.Exit(1)
	}

	images, err := api.Images(ctx)
	if err != nil {
		logger.Error(err, "Failed to get images")
		os.Exit(1)
	}

	for _, i := range images.Resources {
		parts := strings.Split(i.PublicID, "_")
		name := strings.Join(parts[:len(parts)-1], " ")

		fmt.Fprintf(os.Stdout, "%s,%s\n", i.SecureURL, name)
	}
}
